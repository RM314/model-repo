---
MorpheusModelID: M7678

authors: [H. G. Lee, K. J. Lee]
contributors: [L. Brusch]

title: "Collective Motion of MDA-MB-231 Breast Cancer Cells"

# Reference details
publication:
  doi: "10.1371/journal.pcbi.1009447"
  title: "Neighbor-enhanced diffusivity in dense, cohesive cell populations"
  journal: "PLOS Computational Biology"
  volume: 17(9)
  page: "e1009447"
  year: 2021
  original_model: true

tags:
- 2D
- Breast Cancer
- Collective Motion
- Cell Motility
- Cell Migration
- Adhesion
- Persistent Motion
- Spatial Model
- Stochastic Model
- Multicellular Model
- Cellular Potts Model
- CPM

categories:
- DOI:10.1371/journal.pcbi.1009447
---

## Introduction

Collective motion of human breast cancer cells of the cell line MDA-MB-231 was studied by [Lee _et al._](#reference) for individual crawling cells, cell doublets, cell quadruplets and for confluent populations on a two-dimensional substrate.
[Lee _et al._](#reference) found enhanced diffusivity as well as periodic rotation of cell-doublets and cell-quadruplets with mixing events.

Using a simple **Cellular Potts Model** in Morpheus, they showed that active self-propulsion and cell-cell adhesion together are sufficient for reproducing the experimentally observed phenomena of collective motion. 
Interestingly, these cells diffuse more persistently within a densely packed population than when they are free to crawl around individually or in small clusters.

## Model Description

[Lee _et al._](#reference) use a deliberately simple individual-based model with persistent cell motility (parameterized by the strength $S$ of `PersistentMotion`) and cell-cell adhesion (parameterized by the adhesion energy $E$ compared to 0 for medium interfaces) to capture and test two basic mechanisms of MDA-MB-231 breast cancer cells. 
Individual cell shapes are spatially resolved on a two-dimensional lattice to allow studying dynamic rearrangements of cell order within rotating quadruplets.

Mechanics and motility of the spatially resolved cells is govered by the Cellular Potts Model (CPM) 
This Morpheus model was developed with an older version of Morpheus and shared by [Lee _et al._](#reference) together with the publication at [GitHub](https://github.com/josephlee188/Data-for-PLOS-Comp.-Biol.-publication/blob/main/cpm_simulation.xml).
We here tested the model on the latest Morpheus version and added many observables.

To quantify cell order, the dynamically changing center of mass vector `com` is calculated for each cell arrangement. 
Then for each individual cell, the distance vector from `com` to that cell's centroid is calculated and its length output as $d$ ($\mu$m) and its angle as $\theta$ (rad). 
The `time` unit is minute and the `space` unit is micrometer.
The initial cell arrangement can be readily toggled in `CellPopulations`.
The parameter values are set to $S=2.8, E=-65$ as used in Figs. 4-6 in [Lee _et al._](#reference).

## Results

### Published simulation results for cell quadruplet

![](Fig6ab_published.png "Results of model simulations for cell quadruplet, published by [Lee _et al._](#reference) ([Fig. 6a,b](https://doi.org/10.1371/journal.pcbi.1009447.g006)) [*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/)")

### Reproduced results

![Reproduced results from Fig. 6a.](Fig6a_reproduced.png "Reproduced results from Fig. 6a.")

The trajectories of all cells are plotted on the x-y plane with the y-coordinate shifted by time such that the rotations of all cells do no longer overlap and become better visible, also mimicking the three-dimensional plot in Fig. 6a. The color bar appears continuous but should be discrete with just four integer cell IDs.

![Reproduced results from Fig. 6b-top.](Fig6b_reproduced_top.png "Reproduced results from Fig. 6b-top.")

Note the mixing events when one cell increases its angular velocity (steeper portion in angle time course) to overtake another cell (flatter portion in angle time course).

![Reproduced results from Fig. 6b-bottom.](Fig6b_reproduced_bottom.png "Reproduced results from Fig. 6b-bottom.")

The distance of the overtaking cell from the quadruplet's joint center of mass drops sharply as it takes the short cut through the quadruplet.

### Example Movie

![](movie.mp4)

The simulation for the cell quadruplet shows collective rotations and the same mixing events (observe the blue cell overtaking other cells) as in the published Movie S7 by [Lee _et al._](#reference).
