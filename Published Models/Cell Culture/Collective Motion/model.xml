<MorpheusModel version="4">
    <Description>
        <Title>Collective Motion</Title>
        <Details>Full title: Collective Motion of MDA-MB-231 Breast Cancer Cells
Date:	20.08.2022
Authors:	Hyun Gyu Lee, Kyoung J. Lee
Contributor: Lutz Brusch
Software:	Morpheus (open-source), download from https://morpheus.gitlab.io
Time unit:	minute
Space unit:	micrometer
ModelID:	https://identifiers.org/morpheus/M7678
Reference:	This model reproduces the results in this publication in the latest Morpheus version which were originally obtained with the same model in an older Morpheus version:
	Lee HG, Lee KJ (2021) Neighbor-enhanced diffusivity in dense, cohesive cell populations. PLOS Computational Biology 17(9): e1009447.
	https://doi.org/10.1371/journal.pcbi.1009447           
            </Details>
    </Description>
    <Global>
        <Constant symbol="dx" name="1 grid step = 3 µm" value="3"/>
        <Constant symbol="dt" name="1 MCS = 2 min" value="2"/>
    </Global>
    <Space>
        <Lattice class="square">
            <Size symbol="size" value="100, 100, 0"/>
            <Neighborhood>
                <Distance>3</Distance>
            </Neighborhood>
            <BoundaryConditions>
                <Condition type="periodic" boundary="x"/>
                <Condition type="periodic" boundary="y"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <TimeSymbol symbol="time"/>
        <StartTime value="0"/>
        <StopTime value="400"/>
    </Time>
    <CellTypes>
        <CellType name="medium" class="medium"/>
        <CellType name="cancer_cell" class="biological">
            <Property symbol="S" value="2.8"/>
            <Property symbol="tmemory" value="4"/>
            <VolumeConstraint target="100" strength="1"/>
            <SurfaceConstraint target="0.9" strength="1" mode="aspherity"/>
            <PersistentMotion decay-time="tmemory" strength="S"/>
            <VariableVector symbol="com" value="0, 0, 0"/>
            <VectorMapper name="com = center of mass">
                <Input value="cell.center"/>
                <Output symbol-ref="com" mapping="average"/>
            </VectorMapper>
            <Function symbol="d" name="d (µm)">
                <Expression>dx*sqrt((cell.center.x-com.x)^2+(cell.center.y-com.y)^2)</Expression>
            </Function>
            <Function symbol="θ.MinusPiToPi">
                <Expression>atan2(cell.center.y-com.y,cell.center.x-com.x)</Expression>
            </Function>
            <DelayProperty symbol="θ.last" name="θ of last time step" delay="1" value="0"/>
            <Equation symbol-ref="θ.last">
                <Expression>θ.MinusPiToPi</Expression>
            </Equation>
            <Property symbol="θ.turns" value="0"/>
            <Function symbol="θ.unrolled" name="θ (rad)">
                <Expression>θ.MinusPiToPi+θ.turns*2*pi</Expression>
            </Function>
            <System time-step="1" name="detect full turns around com" solver="Euler [fixed, O(1)]">
                <Rule symbol-ref="θ.turns">
                    <Expression>θ.turns + (time>100)*(((θ.MinusPiToPi-θ.last)&lt;(-1.95*pi)) - ((θ.MinusPiToPi-θ.last)>(1.95*pi)))</Expression>
                </Rule>
            </System>
        </CellType>
    </CellTypes>
    <CPM>
        <Interaction>
            <Contact type2="medium" type1="cancer_cell" value="0"/>
            <Contact type2="cancer_cell" type1="cancer_cell" value="-65"/>
            <Contact type2="medium" type1="medium" value="0"/>
        </Interaction>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <MetropolisKinetics temperature="10"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Distance>3</Distance>
            </Neighborhood>
        </ShapeSurface>
    </CPM>
    <CellPopulations>
        <Population type="cancer_cell" size="0">
            <!--    <Disabled>
        <InitCircle random_displacement="0.0" name="cell doublet" mode="regular" number-of-cells="2">
            <Dimensions center="50, 50, 0" radius="10"/>
        </InitCircle>
    </Disabled>
-->
            <InitCircle random_displacement="0.0" name="cell quadruplet" mode="regular" number-of-cells="4">
                <Dimensions center="50, 50, 0" radius="10"/>
            </InitCircle>
            <!--    <Disabled>
        <InitRectangle name="dense population" mode="regular" number-of-cells="990">
            <Dimensions origin="0.0, 0.0, 0.0" size="300, 300, 0.0"/>
        </InitRectangle>
    </Disabled>
-->
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="5">
            <Terminal name="png" size="300 300 0"/>
            <Plot>
                <Cells value="cell.id">
                    <ColorMap>
                        <Color color="red" value="1"/>
                        <Color color="yellow" value="2"/>
                        <Color color="green" value="3"/>
                        <Color color="blue" value="4"/>
                    </ColorMap>
                </Cells>
                <Disabled>
                    <CellArrows/>
                </Disabled>
            </Plot>
        </Gnuplotter>
        <Logger time-step="1">
            <Input>
                <Symbol symbol-ref="time.minutes"/>
                <Symbol symbol-ref="θ.unrolled"/>
                <Symbol symbol-ref="d"/>
                <Symbol symbol-ref="cell.volume"/>
                <Symbol symbol-ref="cell.surface"/>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
                <Disabled/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot>
                    <Style style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis minimum="-size.x/2" maximum="size.x/2">
                        <Symbol symbol-ref="cell.center.xshifted"/>
                    </X-axis>
                    <Y-axis minimum="-size.y/2" maximum="2*size.y">
                        <Symbol symbol-ref="cell.center.yshifted"/>
                    </Y-axis>
                    <Color-bar minimum="1" maximum="3">
                        <Symbol symbol-ref="cell.id"/>
                    </Color-bar>
                    <Range>
                        <Time mode="history" history="300"/>
                    </Range>
                </Plot>
                <Plot>
                    <Style style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="time.minutes"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="θ.unrolled"/>
                    </Y-axis>
                    <Color-bar minimum="1" maximum="3">
                        <Symbol symbol-ref="cell.id"/>
                    </Color-bar>
                    <Range>
                        <Time mode="history" history="300"/>
                    </Range>
                </Plot>
                <Plot>
                    <Style style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="time.minutes"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="d"/>
                    </Y-axis>
                    <Color-bar minimum="1" maximum="3">
                        <Symbol symbol-ref="cell.id"/>
                    </Color-bar>
                    <Range>
                        <Time mode="history" history="300"/>
                    </Range>
                </Plot>
            </Plots>
        </Logger>
        <ModelGraph include-tags="#untagged" format="dot" reduced="false"/>
        <Function symbol="time.minutes" name="t (min)">
            <Expression>(time-100)*dt</Expression>
        </Function>
        <Function symbol="cell.center.yshifted" name="y (µm) shifted with time">
            <Expression>(cell.center.y - size.y/2) * dx + (time-100)*0.7</Expression>
        </Function>
        <Function symbol="cell.center.xshifted" name="x (µm)">
            <Expression>(cell.center.x - size.x/2) * dx</Expression>
        </Function>
    </Analysis>
</MorpheusModel>
