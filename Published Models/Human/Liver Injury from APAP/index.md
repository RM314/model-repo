---
MorpheusModelID: M9496

title: "Detoxification and Liver Injury from Acetaminophen (APAP)"

authors: [M. M. Heldring, A. H. Shaw, J. B. Beltman]
contributors: [L. Brusch]

# Reference details
publication:
  doi: "10.1038/s41540-022-00238-5"
  title: "Unraveling the effect of intra- and intercellular processes on acetaminophen-induced liver injury"
  journal: "NPJ Syst. Biol. Appl."
  volume: 8
  page: 27
  year: 2022
  original_model: true

categories:
- DOI:10.1038/s41540-022-00238-5
  
tags:
- 2D
- Acetaminophen
- Adhesion
- APAP
- Blood Plasma
- Cell Death
- Cell Division
- Cell Migration
- Cellular Potts Model
- Central Vein
- Chemotaxis
- CPM
- Detoxification
- DNA Damage
- DNA Damage Response
- Hepatic Lobule
- Hepatocyte
- Human
- Immune System
- Injury
- Kupffer Cell
- Liver
- Liver Damage
- Liver Injury
- Liver Lobule
- Liver Recovery
- Lobule
- Macrophage
- Mechanistic Model
- Metabolism
- Multicellular Model
- Necrosis
- Necrotic Damage
- ODE
- Partial Differential Equation
- PDE
- Portal Vein
- Proliferation
- Senescence
- Signaling
- Spatial Model
- Stochastic Model
---
> This multiscale mechanistic liver lobule model is composed of various interacting cell types, such as hepatocytes, residential Kupffer cells and macrophages. It captures hepatocellular acetaminophen (APAP) metabolism, DNA damage response activation, necrotic cell death, macrophage recruitment and the regulation of hepatocellular senescence and proliferation, with qualitatively different outcomes as a function of the initial APAP concentration in blood plasma.

{{% callout note %}}
This model also requires the separate file [`hexagon_200.tif`](#downloads).
{{% /callout %}}

## Introduction

The human liver is a major organ of metabolism, is approximately 15 centimeters long and weighs about 1.5 kilograms. It is subdivided into lobes and further into thousands of hexagonal columns, called lobules, which act in parallel. The cross-section of one lobule is shown in Fig. 1a below. 80 % of the lobule volume are occupied by hepatocytes (brown in model figures) that process metabolites from the blood in a spatially cascaded way along the sinusoids, which are blood capillaries running from portal veins (PV, red, in the corners of the lobule) to the central vein (CV, large red circle in Fig. 1a). 
Further cells are residential Kupffer cells (small blue in model figures) and macrophages (green in model figures). Empty, white space in the model represents other non-parenchymal cells, such as sinusoidal epithelial cells and stellate cells, and intercellular spaces.

![](figure_1.png "**Fig. 1:** Schema of liver lobule geometry and APAP metabolism from [Fig. 1](https://www.nature.com/articles/s41540-022-00238-5/figures/1) ([*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/): [**Heldring _et al._**](#reference))")

Pericentral (near CV) hepatocytes are specialized for glycolysis, lipogenesis and cytochrome P450-based drug detoxification. Thus the pericentral hepatocytes are most sensitive to NAPQI production and DNA damage in acetaminophen (APAP) toxicity, see Fig. 1b. Hepatocytes die, are cleared by infiltrating macrophages in response to signals by Kupffer cells and hepatocytes start regenerating over the course of one week after APAP uptake, as shown in the model simulation movie below.

![](movie_published.mp4)
**Video:** Simulation results of the reference model, published as [Suppl. Movie 1](https://www.nature.com/articles/s41540-022-00238-5#Sec19). 
Time is running at the bottom with a unit of $`5\ \mathrm{min}`$. The total duration corresponds to one week.
The color code is red for CV and PVs; brown, healthy hepatocytes; orange, proliferated hepatocytes; gray, senescent hepatocytes; black, necrotic hepatocytes; green, macrophages; blue, Kupffer cells.
([*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/): [**Heldring _et al._**](#reference)).

## Model Description

The CPM is used to model the interacting cell types hepatocytes, residential Kupffer cells and macrophages. PDEs and ODEs are used to model APAP distribution and hepatocellular metabolism, necrotic cell death and macrophage recruitment, DNA damage response activation and the regulation of senescence and proliferation.

Space is discretized as a 2D square lattice with $`3\ \mathrm{\mu m}`$ lattice spacing.
All parameter values are scaled to that space unit of $`3\ \mathrm{\mu m}`$.
Time unit is $`5\ \mathrm{min}`$. One Monte Carlo step (MCS) is set to $`0.00675`$ time units.
The total simulation duration of $`2112`$ time units is split into $`96`$ time units ($`= 6\ \mathrm{h}`$) for initial equilibration before APAP intake and $`2016`$ time units ($`= 168\ \mathrm{h}`$, denoted as real time in graphs) of detoxification and damage response.

The published model was developed with [Morpheus version 2.2.0](/download/2.2.1/) and the here provided and accelerated model was run in the newer Morpheus version 2.3.3 and is being tested with each [latest version of Morpheus](/download/latest).

Below, both model files are shared:

1. {{< model_quick_access "media/model/m9496/model_published.xml" >}}, published as [`08_Mouseliver_default.xml`](https://github.com/lacdr-tox/heldring-spatial-liver-model/blob/Release1/morpheus-models/08_Mouseliver_default.xml) and running in [Morpheus version 2.2.0](/download/2.2.1/)
2. {{< model_quick_access "media/model/m9496/model.xml" >}}, which is running in the [latest version of Morpheus](/download/latest)

The following updates were applied to the published model file to arrive at `model.xml`:
- Explicitly set the duration of one MCS to `mcs_duration`&nbsp;$`= 0.00675`$ as it was implicitly set by Morpheus 2.2.0
- Explicitly set the time steps of `NeighborhoodReporter`s and Plugin propensities to match the originally applied propensities
- Reordered two statements in `CellType` `hepatocytes` to simplify initialization
- Deleted previously disabled `CellPopulation`s to reduce file size
- To speed up runtime and maintain accuracy and original results, the solver settings for ODEs were adjusted and the `time-step` of `Global`:`Event` ‘Start APAP’ was set to $`95`$.


## Results

The main control parameter of the model is the initial APAP blood plasma concentration and encoded by <span title="Global:Constant.symbol = 'Pex_start'">`Pex_start`</span>. The upper limit of its useful parameter range is $`1000\ \frac{\mathrm{\mu g}}{\mathrm{ml}}`$. Below, the model is evaluated for a concentration of $`500\ \frac{\mathrm{\mu g}}{\mathrm{ml}}`$ ($`\approx 10`$-fold higher than a typical human dose).

![](figure_2.png "**Fig. 2:** Schema of intercellular signaling (a) and time courses of cell counts (b) as published in [Fig. 2](https://www.nature.com/articles/s41540-022-00238-5/figures/2) ([*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/): [**Heldring _et al._**](#reference)). The reproduced time course from the updated model in Morpheus version 2.3.3 is shown in (c). Individual stochastic simulations differ slightly as expected.")

![](movie_reproduced.mp4)
**Video:** This movie shows simulation results of the updated {{< model_quick_access "media/model/m9496/model.xml" >}} with [Morpheus version 2.3.3](/download/2.3.3), corresponding to the time courses shown in Fig. 2c above, and reproducing Suppl. Movie 1, shown at the beginning. Also the color code is the same as in the movie above.
