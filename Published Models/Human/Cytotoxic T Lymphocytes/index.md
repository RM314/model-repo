---
MorpheusModelID: M9495

title: "Spatial Effects on Killing Kinetics of Cytotoxic T Lymphocytes"

authors: [R. J. Beck, D. I. Bijker, J. B. Beltman]
contributors: [J. B. Beltman, D. Jahn]

# Reference details
publication:
  doi: "10.1371/journal.pcbi.1007972"
  title: "Heterogeneous, delayed-onset killing by multiple-hitting T cells: Stochastic simulations to assess methods for analysis of imaging data"
  journal: "PLoS Comput. Biol."
  volume: 16
  issue: 7
  page: "1-25"
  year: 2020
  original_model: true

tags:
- 2D
- All Targets at Risk
- Apoptosis
- Cell Death
- CellType
- Cellular Potts Model
- Condition
- CPM
- CTL
- Cytotoxic T Lymphocyte
- delay
- Event
- Heterogeneous Killing Kinetics
- High-Motility CTL 
- Immune Cell
- Immune System
- Infection
- Killing Dynamics
- Killing Kinetics
- Killing Performance
- Low-Motility CTL
- Monte Carlo Simulation
- Multiple Hitting
- Population Level
- Protrusion
- Single Cell Level
- strength
- T Cell
- T Lymphocyte
- Time-Inhomogeneous Killing Activity
- time-step
- trigger = when-true
- Tumor

categories:
- DOI:10.1371/journal.pcbi.1007972
---
{{% callout note %}}
This model also requires the separate file [`boundary.tiff`](#downloads).
{{% /callout %}}

## Introduction

The immune system plays an important role in controlling infections and tumors. Knowledge about the mechanisms through which the immune system accomplishes this can be exploited to develop immunotherapies. A pivotal mechanism involves **killing of target cells by Cytotoxic T Lymphocytes (CTLs)**.

A major limitation of many prior estimates of CTL killing is that analysis is performed on population level data in in vivo settings, with no direct measurements of the killing process. This approach has a number of drawbacks:

1. It can be challenging to accurately **assess the frequency of CTLs and target cells**.
2. **Other immune cells may contribute** to the killing process, confounding estimates of the true CTL killing rate.
3. The processes underlying CTL killing are complex and it may be **insufficient to describe them with a single, time invariant rate constant**.

Besides analysing CTL killing performance at the population level, a potentially useful approach is to analyse CTL killing at the **single cell level**. Such analysis can yield greater insights into the dynamics of the killing process.

Here, stochastic simulations of single CTLs killing small populations of target cells were developed, showing that multiple-hitting can lead to apparently **heterogeneous killing kinetics** between otherwise identical CTLs.

## Description

An agent based **cellular Potts model (CPM)** was employed to generate 2D simulations of CTLs interacting with and killing targets over a period of 12 hours (<span title="Time/StopTime[value] = 43200">`StopTime` = `43200`&nbsp;s</span>). For all CPM simulations the same underlying gamma model of CTL hit generation was maintained as was used for the [Monte-Carlo simulations](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007972#sec010), however several modifications were made that would lead to different (yet not predictable a priori) distributions of hits amongst targets:

- Specifically, instead of allocating hits to all contacted targets with equal probability, **target risk of receiving a hit was proportional to the length of the interface between CTL and target** <span title="CellTypes/CellType[name=&quot;CTL&quot;]/NeighborhoodReporter[name=&quot;ctl contact reporter&quot;]/Output[symbol-ref] = ctl.contact.length">`ctl.contact.length`</span> at the moment of hit generation.
- We also considered the effect of a lower bound on the time (<span title="CellTypes/CellType[name=&quot;target&quot;]/Event[name=&quot;maturing&quot;][time-step] = 60">`time-step` = `60`&nbsp;s</span> with <span title="CellTypes/CellType[name=&quot;target&quot;]/Event[name=&quot;maturing&quot;][trigger] = when-true">`trigger` = `when-true`</span>) required for a CTL to complete a hit, by introducing a **delay condition** <span title="CellTypes/CellType[name=&quot;target&quot;]/Event[name=&quot;hitting&quot;]/Condition[value] = rand_uni(0,1) < hit.chance * (maturity >= minhit) * (1 - is.dead) * ctl.attn">`maturity > minhit`</span> that prohibited targets from being hit within an initial time window after contacting a CTL, which was reset (<span title="CellTypes/CellType[name=&quot;target&quot;]/Event[name=&quot;immaturing&quot;]/Condition[value] = ctl.attn == 0">`immaturing` `Event`</span>) every time the target broke contact with the CTL. Note that the delay condition was applied per target and therefore does not preclude the possibility of CTLs hitting other contacted targets simultaneously.
- We used the CPM model to simulate CTLs (with $\eta,\lambda = 1,2$ or $10$). CTLs hit targets at a constant rate $\lambda$ (intrinsic hitting rate), then targets died after receiving $\eta$ hits (<span title="CellTypes/CellType[name=&quot;target&quot;]/Constant[symbol=nhits][value] = 5">`nhits` = `5`</span>).
- We simulated single- and multiple-hitting scenarios on the basis that the expected (mean) time for one target in contact with a CTL to be killed was 1 hour (<span title="CellTypes/CellType[name=&quot;target&quot;]/Constant[symbol=hit.chance][value] = nhits/3600">`hit.chance` = `nhits/3600`&nbsp;s</span>), i.e., we set $\frac{\lambda}{\eta} = 1$.
- Finally, we varied CTL migration to create two groups of CTLs which we termed **‘high-motility’** (<span title="CellTypes/CellType[name=&quot;CTL&quot;]/Protrusion[strength] = 10">`Protrusion[strength]` = `10`</span>) or **‘low-motility’** (<span title="CellTypes/CellType[name=&quot;CTL&quot;]/Protrusion[strength] = 2">`Protrusion[strength]` = `2`</span>) CTLs.

## Results

![](pcbi.1007972.g003a.png "[Fig. 3A](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007972#pcbi-1007972-g003): Still images of a high motility CTL with 15 minute minimal hitting time, interacting with targets. ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Beck *et al.***](#reference))")

![](pcbi.1007972.g003b-c.png "[Fig. 3B-C](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007972#pcbi-1007972-g003): Track plots showing movement of 3 randomly sampled CTLs of high (B) and low (C) motility throughout a simulation, for simulated $\eta$ as shown. ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Beck *et al.***](#reference))")

Figure 3A shows high-motility in silico CTLs in the CPM simulations:

- **Left color scheme:** CTLs are red, uncontacted targets are grey, and contacted targets have various shades of blue based on their share of total CTL:target interface, which determines their probability of receiving a hit. Targets are overlaid with the number of hits they have received.
- **Right color scheme:** Lattice sites inhabited by the CTL are colored according to actin activity. Targets are black, turning yellow after 15 minutes of continuous contact with the CTL. Elapsed simulation time is displayed in the upper left corner of the stills, presented in minutes since the first frame shown.

![](pcbi.1007972.s009.mp4)
[S1 Video](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007972#sec012): Simulation of **high-motility** CTL, requiring 5 hits to kill targets. ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Beck *et al.***](#reference))

![](pcbi.1007972.s010.mp4)
[S2 Video](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007972#sec012): Simulation of **low-motility** CTL, requiring 5 hits to kill targets. ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Beck *et al.***](#reference))

In all videos, CTLs are shown in red whilst uncontacted targets are in grey. Contacted targets are shaded blue based on their share of total CTL:target interface, i.e. the probability that they will receive the next hit generated by the CTL. Targets are overlaid with the number of hits they have received. Elapsed simulation time (hours:minutes) is displayed in the upper right corner of the videos.

For both motility conditions the migration of the CTLs was influenced by the presence of the targets, as CTLs became corralled by surrounding targets. The difference between these models was that **high-motility CTLs exhibited an increased propensity to break free** from confinement and roam the well. This roaming ensured that over the course of 12 hours the **high-motility CTL made new contacts with far greater frequency than low-motility CTLs** (Fig 3D), although the average number of simultaneously contacted targets at any time was similar (Fig 3E). Thus, the high-motility CTL is expected to approach the previously modeled ‘all targets at risk’ scenario more closely than the low-motility CTLs.

![](pcbi.1007972.g003d-e.png "[Fig. 3D-E](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007972#pcbi-1007972-g003): Frequency at which CTLs form new conjugates (D) and mean number of simultaneously contacted targets per CTL (E) for low- and high-motility CTLs. Plots are based on 100 simulations per condition, with each dot representing one CTL, and circles and error bars indicating mean +/- SD. ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Beck *et al.***](#reference))")

The resulting datasets were visually similar to realistic microscopy data and could be used to investigate methods for recovering the hitting parameters ($\eta$ and $\lambda$) of CTLs from experimental data under various conditions.

- The total amount of targets killed by each CTL depended on the interaction between the parameters $\lambda$ and $\eta$, the CTL motility, and the presence or absence of the delay condition. In particular, the combination of high motility plus 15 minute delay resulted in a substantial decrease in killing in comparison to the other simulation groups, for all values of $\eta$ (Fig 4A).
- Together with the high rate of contact formation in that group (Fig 3D), this is consistent with targets spending significant time in transient contacts with the CTL, too short to result in successful hit delivery.
- The killing rate of the low-motility CTLs was initially greater than of high-motility CTLs, in particular for large $\eta$ (Fig 4B), due to the more stable nature of the contacts leading to greater accumulation of hits among the contacted targets (Fig 4C). 
- High-motility CTLs reduced this deficit over the course of the simulations due to an accumulation of latent hits among uncontacted targets (Fig 4D). These spatial simulations therefore illustrate how CTL:target contact dynamics can play a role in determining killing performance.
- Moreover, since in these models CTLs with the same killing parameters – but different motility parameters – generated different killing kinetics, they are useful to test how underlying killing parameters might be recovered from microscopy data that are similar to data emanating from our realistic simulations.

![](pcbi.1007972.g004a-b.png "[Fig. 4A-B](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007972#pcbi-1007972-g004): A) Mean cumulative killing over time ($\text{CTL}^{-1}$) for CPM simulations of high- and low- motility CTLs&nbsp;($\eta,\lambda = 1,2$ or $10$) B) Mean killing rate ($\text{CTL}^{-1}$) for each simulated condition in A. ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Beck *et al.***](#reference))")

![](pcbi.1007972.g004c-d.png "[Fig. 4C-D](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007972#pcbi-1007972-g004): Mean number of hits received per target, sampled over targets currently contacting the CTL (C) or over targets not currently contacting the CTL (D). ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Beck *et al.***](#reference))")

Here we have used stochastic simulations to show that **‘multiple-hitting’ is a plausible explanation for the heterogeneous and time-inhomogeneous killing activity** recently observed for CTLs in vitro:

- We showed that **multiple-hitting leads to an increase in realised killing rate over time**.
- Moreover, the **extent of this late onset killing increases** when more hits are required to kill targets, or when a greater number of antigen-presenting targets are simultaneously contacted.
- Furthermore, **identical CTLs displayed varying killing performance depending on the number of targets available**.
- Simulating CTLs with variable hitting rates, we also found that the **killing performance of multiple-hitting CTLs is more heterogeneous than killing of single-hitting CTLs**, given similar variation in underlying hitting rate.