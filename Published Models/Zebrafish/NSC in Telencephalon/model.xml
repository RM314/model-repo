<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Title>Lupperger2020</Title>
        <Details>Full title:        Neural stem cell (NSC) redivisions in adult zebrafish telencephalon
Date: 	20.07.2021
Authors: 	V. Lupperger, C. Marr, P. Chapouton
Curators: 	V. Lupperger, L. Brusch
Software: 	Morpheus (open-source), download from https://morpheus.gitlab.io
ModelID: 	https://identifiers.org/morpheus/M2984
Reference:    This model is described in the peer-reviewed publication 
	V. Lupperger, C. Marr, P. Chapouton. Reoccurring neural stem cell divisions in the adult zebrafish telencephalon are sufficient for the emergence of aggregated spatiotemporal patterns. PLoS Biology 18 (12): e3000708, 2020. 
	https://doi.org/10.1371/journal.pbio.3000708
</Details>
    </Description>
    <Global>
        <Constant symbol="p_rediv" value="0.38"/>
        <Variable symbol="c" value="0.0"/>
        <Variable symbol="cellCycleTime" value="1e20"/>
        <Variable symbol="time_of_birth" value="0.0"/>
        <Variable symbol="p" value="2"/>
        <Variable symbol="progenitor" value="0"/>
        <Constant symbol="init_volume" value="50"/>
        <Variable symbol="volume" value="init_volume"/>
        <Variable symbol="minCellSize" value="25"/>
        <Variable symbol="maxCellSize" value="250"/>
        <Variable symbol="clone" value="0"/>
        <Variable symbol="cc" value="0"/>
    </Global>
    <Space>
        <Lattice class="hexagonal">
            <Size symbol="size" value="1500, 1500, 0"/>
            <BoundaryConditions>
                <Condition type="periodic" boundary="x"/>
                <Condition type="periodic" boundary="y"/>
            </BoundaryConditions>
            <Neighborhood>
                <Order>3</Order>
                <!--    <Disabled>
        <Distance>1</Distance>
    </Disabled>
-->
            </Neighborhood>
            <Annotation>spatial unit of one lattice step is \mathrm{\mu m}</Annotation>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime symbol="stoptime" value="1.5e5">
            <Annotation>time unit is \mathrm{0.01 hours}</Annotation>
        </StopTime>
        <TimeSymbol symbol="time"/>
    </Time>
    <CellTypes>
        <CellType class="biological" name="cells">
            <Property symbol="maxCellSize" value="rand_norm(220,50)"/>
            <Property symbol="minCellSize" value="max(init_volume/2,30)"/>
            <Property symbol="volume" value="(maxCellSize-minCellSize)/(1+exp(-0.0005*((time-time_of_birth)-10000)))+minCellSize"/>
            <Property symbol="p" name="proliferation rate" value="9e-6&#xa;"/>
            <Property symbol="c" name="color" value="0"/>
            <VolumeConstraint strength="1" target="volume"/>
            <SurfaceConstraint strength="1" target="0.9" mode="aspherity"/>
            <System solver="Euler [fixed, O(1)]" time-step="1.0">
                <Rule symbol-ref="c">
                    <Expression>if( c > 0, c-1, 0)</Expression>
                </Rule>
                <Rule symbol-ref="volume">
                    <Expression>(maxCellSize-minCellSize)/(1+exp(-0.0005*((time-time_of_birth)-10000)))+minCellSize</Expression>
                </Rule>
                <Rule symbol-ref="progenitor">
                    <Expression>if(progenitor == 0,0,progenitor-1)</Expression>
                </Rule>
                <Rule symbol-ref="cc">
                    <Expression>if(progenitor >0, 2, c>0)</Expression>
                </Rule>
            </System>
            <CellDivision daughterID="daughter" division-plane="random" write-log="csv">
                <Condition>p>rand_uni(0,1) and((time-time_of_birth)==0 or ((time-time_of_birth)>cellCycleTime))</Condition>
                <Triggers>
                    <Rule symbol-ref="c" name="color after division">
                        <Expression>1900</Expression>
                    </Rule>
                    <Rule symbol-ref="p">
                        <Expression>if(mother2daughters&lt;p_rediv,p=1,p=9e-6)
</Expression>
                    </Rule>
                    <Rule symbol-ref="time_of_birth">
                        <Expression>time</Expression>
                    </Rule>
                    <Rule symbol-ref="cellCycleTime">
                        <Expression>(-(ln(rand_uni(0,1)))*85*100)+2215</Expression>
                    </Rule>
                    <!--    <Disabled>
        <Rule symbol-ref="progenitor">
            <Expression>if(daughter ==2 and sym_division >0.9,abs((mother2daughters&lt;0.45)-1)*rint(cellCycleTime),(mother2daughters&lt;0.45)*rint(cellCycleTime))
</Expression>
        </Rule>
    </Disabled>
-->
                    <Rule symbol-ref="minCellSize">
                        <Expression>volume/2</Expression>
                    </Rule>
                    <Rule symbol-ref="maxCellSize">
                        <Expression>rand_norm(220,50)</Expression>
                    </Rule>
                    <!--    <Disabled>
        <Rule symbol-ref="cellCycleTime">
            <Expression>if(daughter==2,rand_norm((-(ln(mother2daughters))*85*100)+2215,300),(-(ln(mother2daughters))*85*100)+2215)</Expression>
        </Rule>
    </Disabled>
-->
                    <Rule symbol-ref="progenitor">
                        <Expression>if(rand_uni(0,1)&lt;0.10,rint(cellCycleTime),0)</Expression>
                    </Rule>
                    <Rule symbol-ref="mother2daughters">
                        <Expression>rand_uni(0,1)</Expression>
                    </Rule>
                    <Rule symbol-ref="m2dProg">
                        <Expression>rand_uni(0,1)</Expression>
                    </Rule>
                </Triggers>
            </CellDivision>
            <CellDeath>
                <Condition>progenitor ==1 or (progenitor >1 and abs((time-time_of_birth)-cellCycleTime) &lt;3 )</Condition>
            </CellDeath>
            <Property symbol="clone" value="rand_uni(1,255)"/>
            <Property symbol="initialId" value="cell.id"/>
            <Property symbol="time_of_birth" value="0.0"/>
            <Property symbol="cellCycleTime" value="(-(ln(rand_uni(0,1)))*85*100)+2215"/>
            <Property symbol="progenitor" value="0.0"/>
            <Property symbol="mother2daughters" value="rand_uni(0,1)"/>
            <Property symbol="sym_division" value="rand_uni(0,1)"/>
            <Property symbol="cc" value="1"/>
            <Property symbol="m2dProg" value="rand_uni(0,1)"/>
        </CellType>
    </CellTypes>
    <CPM>
        <Interaction default="0">
            <Contact type1="cells" type2="cells" value="-4"/>
        </Interaction>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1.0"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <MetropolisKinetics yield="0.1" temperature="3"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>3</Order>
            </Neighborhood>
        </ShapeSurface>
    </CPM>
    <CellPopulations>
        <Population type="cells" size="1">
            <InitCircle number-of-cells="500" mode="random">
                <Dimensions radius="100" center="750,750,0"/>
            </InitCircle>
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="1000" decorate="false">
            <Terminal name="pdf"/>
            <Plot>
                <Cells min="0.0" max="1" value="c">
                    <ColorMap>
                        <Color color="red" value="1"/>
                        <Color color="green" value="0.0"/>
                    </ColorMap>
                </Cells>
                <!--    <Disabled>
        <CellLabels fontsize="4" value="0.0"/>
    </Disabled>
-->
            </Plot>
            <Plot>
                <Cells value="cc">
                    <ColorMap>
                        <Color color="red" value="1"/>
                        <Color color="green" value="0.0"/>
                        <Color color="black" value="2"/>
                    </ColorMap>
                </Cells>
                <!--    <Disabled>
        <CellLabels precision="0" fontsize="1" value="clone"/>
    </Disabled>
-->
            </Plot>
        </Gnuplotter>
        <Logger time-step="1000">
            <Input>
                <Symbol symbol-ref="celltype.cells.size"/>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
                <Symbol symbol-ref="cellCycleTime"/>
                <Symbol symbol-ref="progenitor"/>
                <Symbol symbol-ref="cell.volume"/>
                <Symbol symbol-ref="clone"/>
                <!--    <Disabled>
        <Symbol symbol-ref="minCellSize"/>
    </Disabled>
-->
                <!--    <Disabled>
        <Symbol symbol-ref="maxCellSize"/>
    </Disabled>
-->
                <Symbol symbol-ref="p"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="1000" log-commands="true">
                    <Style style="linespoints" grid="true" point-size="0.5"/>
                    <Terminal terminal="png"/>
                    <X-axis maximum="stoptime" minimum="0">
                        <Symbol symbol-ref="time"/>
                    </X-axis>
                    <Y-axis minimum="0">
                        <Symbol symbol-ref="celltype.cells.size"/>
                    </Y-axis>
                </Plot>
            </Plots>
        </Logger>
        <Gnuplotter time-step="1000" decorate="false">
            <Terminal name="png"/>
            <Plot>
                <Cells value="clone"/>
                <!--    <Disabled>
        <CellLabels precision="0" fontsize="1" fontcolor="white" value="clone"/>
    </Disabled>
-->
            </Plot>
            <Plot>
                <Cells value="cc">
                    <ColorMap>
                        <Color color="red" value="1"/>
                        <Color color="green" value="0.0"/>
                        <Color color="black" value="2"/>
                    </ColorMap>
                </Cells>
                <!--    <Disabled>
        <CellLabels precision="0" fontsize="1" value="clone"/>
    </Disabled>
-->
            </Plot>
        </Gnuplotter>
        <!--    <Disabled>
        <DependencyGraph format="svg"/>
    </Disabled>
-->
        <ModelGraph include-tags="#untagged" format="dot" reduced="false"/>
    </Analysis>
</MorpheusModel>
