---
MorpheusModelID: M2053

title: "Single Genotype Circuit"

authors: [N. Mulberry, L. Edelstein-Keshet]
contributors: [N. Mulberry]

# Under review ('true') or publicly listed ('false')
#hidden: false
#private: false

# Reference details
publication:
  doi: "10.1088/1478-3975/abb2dc"
  title: "Self-organized Multicellular Structures from Simple Cell Signaling: A Computational Model"
  journal: "Phys. Biol."
  volume: 17
  #issue:
  page: "066003"
  year: 2020
  published_model: original
  #preprint: false

tags:
- Cadherin-1
- CDH1
- Cell Differentiation 
- Cell Sorting
- Cellular Potts Model
- CPM
- Differential Adhesion
- E-cadherin
- Emergent Behavior
- Intercellular Signaling
- Intracellular Signaling
- Lateral Inhibition
- Multiscale Models
- Notch Signaling Pathway
- Self-organization
- Spatio-temporal Rearrangement
- Two-layer Structure

categories:
- DOI:10.1088/1478-3975/abb2dc
---
## Introduction

This model combines simple Notch signaling with the cellular Potts model (CPM), which drives cell sorting as a result of differential adhesion.

Model details are contained in the reference [Mulberry and Keshet, 2020](#reference). Our computational model was inspired by the experimental work of [Toda et al., 2018](https://science.sciencemag.org/content/361/6398/156). The Notch signaling model was based on that of [Boareto et al., 2015](https://www.pnas.org/content/112/5/E402.short).

In this experiment, we consider a mixture of cells of a single genotype. These cells can become ‘activated’ through lateral inhibition and eventually self-organize into a stable two-layer structure.  

## Description

We model a mixture of cells which all have both sender and receiver capabilities:  
  
![](schematic_singlegenotype.png)

The full Delta-Notch dynamics are given by the following system of differential equations:  

$$\begin{align}
\frac{\mathrm dN}{\mathrm dt} &= N_0 \left(1 + \frac{I^p}{I_0^p+I^p}\right) - \kappa_cND - \kappa_tND_\text{ext}-\gamma N \\\\
\frac{\mathrm dI}{\mathrm dt} &= \kappa_tND_\text{ext}- \gamma_I I\\\\
\frac{\mathrm dE}{\mathrm dt} &= E_0 \frac{I^p}{I_0^p+I^p} - \gamma E\\\\
\frac{\mathrm dD}{\mathrm dt} &= D_0 \left(1 + \frac{I^p}{I_0^p+I^p}\right) - \kappa_tN_\text{ext}D - \gamma D \\\\
\end{align}$$
  
where <span title="//CellTypes/CellType[@name='A']/Property[@symbol='N']">$N$</span> denotes the Notch level in each cell, <span title="//CellTypes/CellType[@name='A']/Property[@symbol='D']">$D$</span> denotes the Delta level in each cell, <span title="//CellTypes/CellType[@name='A']/Property[@symbol='I']">$I$</span> is the Notch intracellular domain, and finally <span title="//Global/Variable[@symbol='E']">$E$</span> is the cell's E-cad production. <span title="//CellTypes/CellType[@name='A']/Property[@symbol='Nn']">$N_\text{ext}$</span> and <span title="//CellTypes/CellType[@name='A']/Property[@symbol='Dn']">$D_\text{ext}$</span> are the average Notch and Delta levels of the cell's immediate neighbourhood, respectively.

## Results

Cells (initially all red) aggregate into a single cluster quickly and are gradually induced by neighbors to differentiate into more adhesive (green) E-cadherin expressing cells, forming differentiated structures. The structures are dynamic, with cells continuously changing type. Since the process of cell differentiation continues (i.e. cell states are not fixed), the structure is imperfect; however, once a cell differentiates, it is quickly sorted into its respective layer. 

![](SingleGenotype_Mov10.gif "Single Genotype Circuit")