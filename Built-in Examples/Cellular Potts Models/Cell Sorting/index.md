---
MorpheusModelID: M0021
aliases: [/examples/differential-adhesion-cell-sorting-in-two-dimensions/]

title: "Differential Adhesion: Cell Sorting in Two Dimensions"

authors: [F. Graner, J. A. Glazier]

# Under review
#hidden: true
#private: true

# Reference details
publication:
  doi: "10.1103/PhysRevLett.69.2013"
  title: "Simulation of biological cell sorting using a two-dimensional extended Potts model"
  journal: "Phys. Rev. Lett."
  volume: 69
  issue: 13
  page: "2013-2016"
  year: 1992
  #original_model: true

# Manual order
menu:
  Built-in Examples:
    parent: Cellular Potts Models
    weight: 10
weight: 110

tags:
- CellPopulation
- Cell Sorting
- CellType
- Cellular Potts Model
- CPM
- Differential Adhesion
- Differential Adhesion Hypothesis
- Engulfment
- Glazier-Graner-Hogeweg Model
- Glazier-Graner Model
- InitCircle
- Initial Condition
- Interaction
- Intermediate State
- MetropolisKinetics
- Mosaic
- Population
- Reorganization
- Result State
- SaveInterval
- Snapshot
- Steinberg
- Target Area
- Target Volume
- VolumeConstraint

#categories:
#- DOI:10.1103/PhysRevLett.69.2013
---
## Introduction

This model shows the original cellular Potts model (a.k.a. [Glazier-Graner model](#reference)) of cell sorting based on the Steinberg's differential adhesion hypothesis.

![](cellsorting-2d.png "Yellow cells engulf the red cells as a result of differential adhesion.")

## Description

Two <span title="//CellTypes/CellType[@name='ct1' or @name='ct2']">`CellType`</span>s are defined, each of which has a <span title="//CellTypes/CellType[@name='ct1' or @name='ct2']/VolumeConstraint">`VolumeConstraint`</span> specifying the cell's target area/volume. In the <span title="//CPM">`CPM`</span> element, the <span title="//CPM/MonteCarloSampler/MetropolisKinetics">`MetropolisKinetics`</span> can be configured and the <span title="//CPM/Interaction">`Interaction`</span> energies between cell types are specified.

## Results

The simulation shows two populations of spatially resolved cells that initially have organized in a mosaic fashion. Through differential adhesion, the motile cells sort out and re-organize into an distribution in which one cell type engulfes the other.

<figure>
<div style="padding:75% 0 0 0;position:relative;">
<iframe src="https://player.vimeo.com/video/47171579?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
</div><script src="https://player.vimeo.com/api/player.js"></script>
<figcaption>Simulation video of <code>CellSorting_2D.xml</code></figcaption>
</figure>

{{% callout note %}}
Snapshots of the simulation can be saved to files named `[Title][Time].xml.gz` using the <span title="//Time/SaveInterval/@value">`SaveInterval`</span> component, which is by default disabled in {{< model_quick_access "media/model/m0021/CellSorting_2D.xml" >}}. These files containing intermediate and result states can be opened and used as initial conditions for new simulations.

Instead of initializing cells as single points, e.g. with the <span title="//CellPopulations/Population[@type='ct1' or @type='ct2']/InitCircle">`InitCircle`</span> plugin, the restored results of a previous simulation explicitely specify the <span title="//CellPopulations/Population/Cell/Nodes">`Nodes`</span> of each <span title="//CellPopulations/Population/Cell">`Cell`</span> in the <span title="//CellPopulations/Population">`CellPopulations`</span>.

Remember to change <span title="//Time/StartTime/@value">`StartTime`</span> and <span title="//Time/StopTime/@value">`StopTime`</span> accordingly.
{{% /callout %}}