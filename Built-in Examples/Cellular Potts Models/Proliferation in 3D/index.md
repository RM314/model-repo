---
MorpheusModelID: M0024

title: "Proliferation in Three Dimensions"
date: "2019-11-06T17:39:00+01:00"
lastmod: "2020-10-30T12:43:00+01:00"

aliases: [/examples/proliferation-in-three-dimensions/]

menu:
  Built-in Examples:
    parent: Cellular Potts Models
    weight: 40
weight: 140
---

## Introduction

This model shows a CPM simulation of a growing cell population in 3D.

![](proliferation-3d.png "Cell population grown from single initial cell.")

## Description

The model specifies ```CellType``` which has a ```VolumeConstraint``` and a ```Proliferation``` plugin.  In the ```Proliferation``` plugin, the ```Conditions``` for a cell to divide are given. Here, each cell that has more than 90% of the target volume has a small probability to divide. Once a division has taken place, the ```Equation``` defined in the ```Triggers``` elements are triggered.

In this model, two ```medium``` cell types have been defined. One of these (called *matrix*) is used to represent a matrix with higher adhesivity. This is done by

1. defining the *matrix* cell type as a ```BoundaryCondition``` of the $-z$ boundary in the ```CPM``` and
1. providing lower contact energy for cell-matrix interaction than for cell-medium interactions.

The simulation is visualized using the ```TiffPlotter``` that saves TIFF image stacks that can be loaded by image analysis software such as [Fiji](http://fiji.sc) and displayed using Fiji's 3D Viewer plugin.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/47313357?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>