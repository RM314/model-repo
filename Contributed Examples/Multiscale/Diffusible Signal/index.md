---
MorpheusModelID: M5491

contributors: [L. Brusch]

title: Mass-conserving secretion and uptake of a diffusible signal
date: "2023-11-09T10:42:00+02:00"
lastmod: "2023-11-09T14:49:00+01:00"

tags:
- 2D
- Adhesion
- Cellular Potts Model
- Chemotaxis
- CPM
- Mass Conservation
- Multicellular Model
- Partial Differential Equation
- PDE
- Secretion
- Signaling
- Spatial Model
- Stochastic Model
- Uptake

---

## Introduction

The mass-conserving secretion and uptake of (signaling) molecules by cells can in Morpheus be achieved with ```Mappers``` and scaling fluxes by ```cell.volume```. The intracellular amount of signal may trigger a change in a state variable that could downstream change cell behaviors. 

This model, motivated by the [user forum](https://groups.google.com/g/morpheus-users/c/NP0sSKlvMRc), shall demonstrate how such signaling processes can be coupled via a global field of diffusible molecules and how conservation of mass can be monitored over populations of cells. 

## Description

There are two cell types (besides medium): ```cell_secreting``` (which secretes signal ```U``` with flux ```p``` into the ```Global/Field``` ```U.external```) and ```cell_receiving``` (which takes ```U``` up from ```U.external``` with rate ```d``` and accumulates it in ```U.internal```, when ```U.internal``` exceeds ```U.threshold=500``` then ```cell_receiving``` changes its state ```U.superthreshold``` from 0 to 1). The chosen CPM settings let cells of different types repel each other and let ```cell_receiving``` chemotax upward the ```U.external``` gradient. The number (0 or 1) plotted in each receiving cell is the state of ```U.superthreshold```.

Please also see the descriptions of related models like [Autocrine chemotaxis](https://identifiers.org/morpheus/M0031) and the cAMP dynamics during [Dictyostelium aggregation](https://identifiers.org/morpheus/M0034). Moreover, the field ```U.external``` may get extra reaction terms, see this [Example](https://identifiers.org/morpheus/M0012).

![](plot_00020.png "Snapshot at time=20 showing the initial cell states (secreting cells in green and without any number, receiving cells in gray with the number 0).")

![](plot_00500.png "Snapshot at time=500 showing the concentration fields change (yellow/red colors in background with contour lines) and changed amounts of signaling molecules inside cells as given by the color bar.")

![](movie.mp4)
The video shows how the secreting cells (initially green and without any number) lose the signal (corresponding to color bar), how the concentration fields change (yellow/red colors in background with contour lines) and receiving cells (with a number) take up the signal (change color from gray to blue and then green corresponding to color bar). When a threshold of internalized signal is crossed, then receiving cells change their state from 0 to 1 (shown as number inside receiving cells). Receiving cells chemotax upward in the gradient of the concentration field.

![](logger.png "Time courses of sums of signals in all secreting, receiving cells as well as the extracellular field show that the total amount is conserved.")
