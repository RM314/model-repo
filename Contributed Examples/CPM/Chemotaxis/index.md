---
MorpheusModelID: M2010

title: "Chemotaxis"

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true

tags:
- 2D
- Boundary
- BoundaryValue
- Cell Motion
- Cell Trajectory
- Cellular Potts Model
- Chemical Attractant
- Chemoattractant
- Chemotaxis
- CPM
- Decay
- DiffEqn
- Diffusion
- Domain
- Domain Boundary
- Drift
- Field
- Gradient
- Heat Map
- Keller-Segel Chemotaxis
- Keller-Segel Cells
- PDE
- Secretion
- Trajectory

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> Diffusion model with cells endowed with chemotactic ability

## Introduction

Previously developed diffusion models can be used to model chemotaxis by adding cells endowed with chemotactic ability. Here, chemotaxis of a group of cells towards a chemical diffusing into the domain with a high level at the right boundary is demonstrated, as well as in a second model, Keller-Segel cells being the sources of their own attractant.
## Description

Cells are endowed with the property of <span title="//CellTypes/CellType/Chemotaxis[@field='c'][@strength='chi']">`Chemotaxis`</span> with some rate <span title="//CellTypes/CellType/Constant[@symbol='chi'] or //CellTypes/CellType/Function[@symbol='chi']">$`\chi`$</span> towards a chemical <span title="//Global/Field[@symbol='c']">$`c`$</span>.

### Chemotaxis of Cells towards Domain Boundary

In {{< model_quick_access "media/model/m2010/SimpleChemotaxis_main.xml" >}}, the chemical level is initially high (<span title="//Global/Field/BoundaryValue[boundary='x']/@value">`BoundaryValue`&nbsp;$`= 5.0`$</span>) only along the right boundary of the domain. <span title="//Global/Field[@symbol='c']/Diffusion">`Diffusion`</span> and a <span title="//Global/System/DiffEqn[@symbol-ref='c']">`DiffEqn`</span> for decay then result in the gradient from left to right that directs the cell motion.

### Keller-Segel Chemotaxis

In {{< model_quick_access "media/model/m2010/KellerSegelChemotaxis.xml" >}}, the cells are sources of their own attractant, and move by chemotaxis in response to that attractant. The secreted chemical then diffuses and decays in the domain.
## Results

![](SimpleChemotaxis.png "Chemotaxis of cells towards a chemical attractant $`c`$ whose gradient is established by diffusion from the right boundary. Shown are the cells (bottom panels) at $`t = 100, 200, 300`$ time steps (left to right). The level of $`c`$ is indicated by red (high) to yellow (low) shades. Cells started at $`t = 0`$ in the small circle shown in the bottom left panel. The trajectories of the cells from $`t = 0`$ up to the given times are shown on the top panels. Produced with Morpheus file [`SimpleChemotaxis_main.xml`](#model).")

<figure>
  ![](M2010_chemotaxis_movie_SimpleChemotaxis_main.mp4)
  <figcaption>
    Simulation video of {{< model_quick_access "media/model/m2010/SimpleChemotaxis_main.xml" >}} showing chemotaxis of cells towards the chemical attractant
  </figcaption>
</figure>

<figure>
  ![](M2010_chemotaxis_movie_SimpleChemotaxis_main_trajectories.mp4)
  <figcaption>
    Simulation video of {{< model_quick_access "media/model/m2010/SimpleChemotaxis_main.xml" >}} showing the trajectories of the attracted cells
  </figcaption>
</figure>

![](KellerSegelChemoSim.png "A simulation of the Keller-Segel chemotaxis, illustrating the aggregation of cells that produce a chemical and are attracted by it. A time sequence from left to right and top to bottom starting with time $`t = 5`$ in increments of $`\Delta t = 15`$. The level of the chemical attractant is shown in shades from white to yellow and red as its concentration increases. Cells are all identical, but colored differently for ease of tracking their motions. Produced by Morpheus file [`KellerSegelChemotaxis.xml`](#downloads).")

<figure>
  ![](M2010_chemotaxis_movie_KellerSegelChemotaxis.mp4)
  <figcaption>
    Simulation video of {{< model_quick_access "media/model/m2010/KellerSegelChemotaxis.xml" >}}
  </figcaption>
</figure>
