---
MorpheusModelID: M2016

title: Agent-based Model

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true
#  preprint: false

tags:
- ABM
- Agent-based Model
- Cell Tracking
- Cellular Potts Model
- CPM
- DirectedMotion
- DisplacementTracker
- Local Model
- Mean Squared Displacement
- MSD
- Neural Activity
- Non-local Model
- Random Motion
- Random Walk
- Random Walker
- Run and Tumble
- Single Cell
- Trajectory
- Unbiased Random Walk

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> An agent-based model where each cell's position and velocity is tracked

## Introduction

We expand our exploration of local and non-local models to the class of agent-based models.

## Description

Instead of tracking continuous distributions of cell density, neural activity, or other properties that vary continuously across space, we adopt a different kind of modeling approach by tracking the trajectories and behavior of individual cells.

For this, the mean squared displacement (MSD) of <a href="/model/M2003" title="<i class='fas fa-dna pr-1'></i>Morpheus Model ID: <code class='model-id-tooltip'>M2003</code>">random walkers in 2D</a> is calculated and plotted using a <span title="//CellTypes/CellType[@name='Walker']/Mapper/Output[@symbol-ref='MSD']">`Mapper`</span>.

## Results

![](ABMChapHeader.png "The cells spread out (left to right). Each cell's position and velocity is tracked along with their trajectories plotted in the right panel.")

<figure>
    ![](M2016_agent-based-model_movie.mp4)
    <figcaption >
        Video of the simulation of {{< model_quick_access "media/model/m2016/ABMmodel.xml" >}} showing individual cells running and tumbling throughout the entire two-dimensional domain.
    </figcaption>
</figure>
