---
MorpheusModelID: M2006

title: "Two CPM Cells Adhesion"

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true

tags:
- Adhesion
- Adhesion Energy
- Cell-cell Contact
- Cell-medium Contact
- Contact
- Contact Energy
- Cellular Potts Model
- CPM
- Interaction
- Interaction Energy
- Intercellular Contact
- Medium
- SurfaceConstraint
- VolumeConstraint

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> Example demonstrating how adhesion energies affect cell shape and the cell-cell contact region

## Introduction

We simulate two CPM cells of the same type to demonstrate how adhesion energies affect cell shape and the cell-cell contact region.
## Description

Let <span title="//CPM/Interaction/Contact[@type1='amoeba'][@type2='amoeba']/@value">$`J_\mathrm{aa}`$</span> be the adhesion energy between the two cells, and <span title="//CPM/Interaction/Contact[@type1='amoeba'][@type2='medium']/@value">$`J_\mathrm{am}`$</span> be the adhesion energy between each of the cells and the medium.

## Results

The following diagram demonstrates that when the cell-cell contact energy is too high, the cells separate. When the cell-medium contact energy is too high, the cells make as large a region of contact as possible, while also attempting to satisfy their own volume (<span title="//CellTypes/CellType[@name='amoeba']/VolumeConstraint/@target">`VolumeConstraint`</span>) and perimeter constraints (<span title="//CellTypes/CellType[@name='amoeba']/SurfaceConstraint/@target">`SurfaceConstraint`</span>).
![](TwoCPMCellsAdhesion.png "Examples of the effect of cell- cell and cell-medium adhesion energies on the shape and contact region between two CPM cells.")

{{% callout note %}}
As shown in the figure above, **try the following <span title="//CPM/Interaction/Contact[@type1='amoeba'][@type2='amoeba' or @type2='medium']/@value">`Contact`</span> settings**, which default to case a), in the CPM main section of the [Morpheus GUI](/courses/getting-started/have-a-look-around/#model-construction-interface):

| Behavior | Cell-cell <span title="//CPM/Interaction/Contact[@type1='amoeba'][@type2='amoeba']/@value">`Contact` ($`J_\mathrm{aa}`$)</span> | Cell-medium <span title="//CPM/Interaction/Contact[@type1='amoeba'][@type2='medium']/@value">`Contact` ($`J_\mathrm{am}`$)</span> |
|:---:|--------------------:|----------------------:|
| **a)** |              $`10`$ |                 $`4`$ |
| b) |               $`4`$ |                 $`2`$ |
| c) |               $`2`$ |                 $`4`$ |
| d) |               $`4`$ |                $`10`$ |

{{% /callout %}}

<figure>
    ![](M2006_two-cpm-cells-adhesion_movie_behavior-a.mp4)
    <figcaption>Simulation video of <a href="#model"><code>TwoCPMCellsAdhesion.xml</code></a> showing the preset behavior a) with <span title="//CPM/Interaction/Contact[@type1='amoeba'][@type2='amoeba']/@value">$`J_\mathrm{aa} = 10`$</span> and <span title="//CPM/Interaction/Contact[@type1='amoeba'][@type2='medium']/@value">$`J_\mathrm{am} = 4`$</span></figcaption>
</figure>