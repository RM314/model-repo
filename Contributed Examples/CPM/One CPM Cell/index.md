---
MorpheusModelID: M2002

title: "One CPM Cell"

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true

tags:
- Cellular Potts Model
- ConnectivityConstraint
- Contact
- CPM
- Deformation
- Irregular Cell Shape
- Irregular Shape
- MetropolisKinetics
- SurfaceConstraint
- Single Cell
- Target Area
- Target Perimeter
- Temperature
- Time-step
- VolumeConstraint
- Yield

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
>Time sequence showing the changing shape of a CPM cell

## Introduction

Not all cell shapes are circular. In this example, a single cell undergoes a whole series of deformations resulting in the development of irregular cell shapes.

## Description

For a time sequence showing the shape of a CPM cell every $`50`$ <span title="//Analysis/Gnuplotter/@time-step">`time-step`</span>s the following parameters were used:

- Target area (<span title="//CellTypes/CellType[@name='amoeba']/VolumeConstraint/@target">`VolumeConstraint`</span>) $`a_0 = 400`$,
  - <span title="//CellTypes/CellType[@name='amoeba']/VolumeConstraint/@strength">`strength`</span> $`\lambda_a = 1`$,
- target perimeter (<span title="//CellTypes/CellType[@name='amoeba']/SurfaceConstraint/@target">`SurfaceConstraint`</span>) $`p_0 = 120`$,
  - <span title="//CellTypes/CellType[@name='amoeba']/SurfaceConstraint/@strength">`strength`</span> $`\lambda_p = 1`$,
- <span title="//CellTypes/CellType[@name='amoeba']/ConnectivityConstraint">`ConnectivityConstraint`</span> enabled,
- cell-medium <span title="//CPM/Interaction/Contact[@type1='amoeba'][@type2='medium']/@value">`Contact`</span> strength&nbsp;$` = 2`$,
- <span title="//CPM/MonteCarloSampler/MetropolisKinetics/@temperature">`temperature`&nbsp;$`= 1`$,
- <span title="//CPM/MonteCarloSampler/MetropolisKinetics/@yield">`yield`&nbsp;$`= 0.1`$.

## Results

The cell continuously puts out protrusions in various directions. 

![](OneCPMCell.png "A time sequence showing the shape of a CPM cell every $`50`$ time steps.")

<figure>
![](M2002_one-cpm-cell_movie.mp4)
<figcaption>Simulation video of <a href="#model"><code>OneCPMCell.xml</code></a></figcaption>
</figure>