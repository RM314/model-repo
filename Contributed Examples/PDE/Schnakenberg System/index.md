---
MorpheusModelID: M2009

title: "Schnakenberg System"

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true

tags:
- 1D
- 2D
- BoundaryCondition
- Domain
- Image
- Irregular Domain
- Noflux
- Schnakenberg System
- Partial Differential Equation
- PDE
- Reaction-diffusion System

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> Creating patterns using the Schnakenberg reaction-diffusion system 

## Introduction

We use the Schnakenberg system to generate patterns using a pair of reaction-diffusion (RD) equations, build up from 1D to 2D and explore parameter dependence.

## Description

We run the Schnakenberg system of PDEs in 1D with no-flux <span title="//Space/Lattice/BoundaryConditions/Condition">`BoundaryConditions`</span> from initial profile of noise.

## Results

![](Schnak1DAI.png "The final pattern (at <span title='//Time/StopTime/@value'>$`t = 500`$</span>) in the 1D Schnakenberg system.")

![](Schnak1DtimeSeq.png "A time sequence of pattern formation in the 1D Schnakenberg system, the simulation starts with random initial conditions and proceeds to form a series of peaks.")

![](SchnakPatternsA.png  "Top: a time sequence of the Schnakenberg RD system equation. Stripes are initialized, merged and sharpened due to the pattern-forming system; produced by [`Schnakenberg2Da.xml`](#downloads). Bottom: The same RD system but with random noise close to the HSS as initial conditions. A pattern of spots emerges over time, produced with [`Schnakenberg2Db.xml`](#downloads).")

![](SchnakPatternsVaryGamma.png "A variety of patterns formed by the Schnakenberg RD system with noisy initial conditions but with various values of the time-scale parameter <span title='//Global/Constant/[@symbol=&#39;gamma&#39;]'>$`\gamma`$</span>. In each case, the system was integrated until <span title='//Time/StopTime/@value'>$`t = 1000`$</span> using the Morpheus file [`Schnakenberg2Db.xml`](#downloads).")

![](GrowingIrregDomain.png "Simulations of the Schnakenberg system on different sized irregular domains with <span title='//Space/Lattice/BoundaryConditions/Condition[@boundary=&#39;x&#39; or @boundary=&#39;-x&#39; or @boundary=&#39;y&#39; or @boundary=&#39;-y&#39;]/@type'>`noflux`</span> <span title='//Space/Lattice/BoundaryConditions/Condition'>`BoundaryCondition`</span>s. Produced with Morpheus file [`Schnakenberg2Dshape.xml`](#downloads).")

{{% callout note %}}
The model {{< model_quick_access "media/model/m2009/Schnakenberg2Dshape.xml" >}} also requires the separate files [`picture1.tiff`](#downloads), [`picture2.tiff`](#downloads) and [`picture3.tiff`](#downloads).
{{% /callout %}}