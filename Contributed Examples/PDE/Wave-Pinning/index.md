---
MorpheusModelID: M2011

title: Wave-Pinning

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true

tags:
- 1D
- 2D
- Actin
- Blood
- Blood Cell
- BoundaryConditions
- Cell Membrane
- Conservation
- Convex Region
- Curvature
- Cytoskeleton
- Cytosol
- Domain
- Edge Curvature
- Field
- Gnuplotter
- GTPase
- High Activity Zone
- Hotspot
- Image
- Interface Length
- Irregular Domain
- Isoline
- Kymograph
- Neutrophil
- Noflux
- Oval Domain
- Partial Differential Equation
- PDE
- Rectangular Domain
- Small GTPase
- Spatial Distribution
- Steady State
- Wave
- Wave-Pinning
- Well-mixed

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> Spatial distribution of GTPase activity

## Introduction

The actin cytoskeleton is regulated by a set of proteins called Small GTPases. The spatial distribution of GTPase activity changes on a timescale of seconds in rapidly moving cells like white blood cells (neutrophils). Here, we generalize a well-mixed GTPase model system to include spatial distribution.

In this context, we investigate the influence of the conservation of the total GTPase and discuss the different behaviors that occur in various domains such as

- traveling waves for a constant pool of inactive GTPase in 1D,
- stalling wave (wave-pinning) for constant total GTPase in 1D,
- stalling wave in a 2D rectangular domain,
- stalling wave in a 2D oval and irregular domain,
- effects of edge curvature on wave-pinning behavior in 2D domains.

## Description

Consider the equations depicting a GTPase model, choosing $`u`$ as it's active and $`v`$ as the inactive form:

$$\begin{align}
\frac{\partial u}{\partial t} &= (b + \gamma \frac{u^n}{1+u^n})v - u + D_u\Delta u \\\\
\frac{\partial v}{\partial t} &= - (b + \gamma \frac{u^n}{1+u^n})v + u + D_v\Delta v \\\\
\end{align}$$

## Results

![](1GTPase1PDE1DnoCons.png "A simulation of the GTPase model in 1D, with no-flux <span title='//Space/Lattice/BoundaryConditions/Condition[@type=&#39;noflux&#39;]'>`BoundaryConditions`</span> and a constant pool of inactive form <span title='//Global/Field[symbol=&#39;i&#39;]/@value'>$`\nu = 2`$</span>. A wave that is initiated at the left boundary sweeps all the way across the domain so that the entire cell has a uniform GTPase level of activity (spatial profiles depicted at $`t = 0, 15, 30, 45, 60`$). Produced with the Morpheus file [`NotWavepinning_main.xml`](#model).")

<figure>
  ![](M2011_wave-pinning_movie_NotWavepinning_main.mp4)
  <figcaption>
    Simulation video of {{< model_quick_access "media/model/m2011/NotWavepinning_main.xml" >}}
  </figcaption>
</figure>

![](2PDE1GTPase1D_WP.png "As in the figure above, but for constant total GTPase (2 PDEs), demonstrating wave-pinning behavior. In contrast to the figure above, the waves only move part ways into the domain, so that a spatially nonuniform steady state is created. The right part of the 1D domain has low GTPase activity, whereas the left has high activity. The same result is shown in the kymograph (lower right panel). Produced with the Morpheus file [`WavePinning2PDEsConservedTotal1D.xml`](#downloads).")

<figure>
  ![](M2011_wave-pinning_movie_WavePinning2PDEsConservedTotal1D.mp4)
  <figcaption>
    Simulation video of {{< model_quick_access "media/model/m2011/WavePinning2PDEsConservedTotal1D.xml" >}}
  </figcaption>
</figure>

![](WP_in_2DMorpheus.png "A simulation of the wave-pinning GTPase model in 2D on a rectangular domain, stimulated at the lower left corner. Shown are the plots for $`t = 0, 50, 200, 500`$ time steps together with level curves of the level of active GTPase. The total GTPase is conserved. Produced with the Morpheus file [`Wavepinningin2D.xml`](#downloads).")

<figure>
  ![](M2011_wave-pinning_movie_Wavepinningin2D.mp4)
  <figcaption>
    Simulation video of {{< model_quick_access "media/model/m2011/Wavepinningin2D.xml" >}}
  </figcaption>
</figure>

![](WP_2D_Oval.png "A simulation of the  wave-pinning GTPase model in 2D on an oval shaped domain, with random initial conditions and no-flux <span title='//Space/Lattice/BoundaryConditions/Condition[@type=&#39;noflux&#39;]'>`BoundaryConditions`</span>. Shown are the plots for $`t = 0, 500, 5000, 5500`$ with activated <span title='//Analysis/Gnuplotter/Plot/Field[@isolines=&#39;5&#39;]'>`isolines`</span> (red). Produced with the Morpheus file [`Wavepinning2DOvalDomain.xml`](#downloads).")

In the simulation shown in the figure above, the system quickly forms hotspots at the two opposite poles of the domain. These persist for a long time. One peak eventually wins and the other disappears.

<figure>
  ![](M2011_wave-pinning_movie_Wavepinningin2DOvalDomain.mp4)
  <figcaption>
    Video of another simulation run of {{< model_quick_access "media/model/m2011/Wavepinning2DOvalDomain.xml" >}}
  </figcaption>
</figure>

{{% callout note %}}
The model {{< model_quick_access "media/model/m2011/Wavepinning2DOvalDomain.xml" >}} also requires the separate file [`Rodlarger.tiff`](#downloads).
{{% /callout %}}

![](WPIrreg2DDomain.png "A simulation of the wave-pinning GTPase model in an irregular domain with no-flux <span title='//Space/Lattice/BoundaryConditions/Condition[@type=&#39;noflux&#39;]'>`BoundaryConditions`</span> starting with random intial conditions. Shown is the level of active form and the level curves for GTPase for times $`t = 0, 100, 200, 600`$ (top row) and $`t = 1000, 2000, 3000, 4000`$ (bottom row) with <span title='//Analysis/Gnuplotter/Plot/Field[@isolines=&#39;5&#39;]'>`isolines`</span> (red) enabled. Produced with the Morpheus file [`Wavepinningin2DIregDomain.xml`](#downloads).")

<figure>
  ![](M2011_wave-pinning_movie_Wavepinningin2DIregDomain.mp4)
  <figcaption>
    Video of another simulation run of {{< model_quick_access "media/model/m2011/Wavepinningin2DIregDomain.xml" >}}
  </figcaption>
</figure>

{{% callout note %}}
The model {{< model_quick_access "media/model/m2011/Wavepinningin2DIregDomain.xml" >}} also requires the separate file [`domainLarge.tiff`](#downloads).
{{% /callout %}}

In the figure above, produced with the Morpheus file {{< model_quick_access "media/model/m2011/Wavepinningin2DIregDomain.xml" >}}, the intial dynamics quickly set up a few ‘hotspots’ that gradually merge and/or attach to the domain boundary. On a much slower time-scale, the high activity zones gradually evolve to minimize their interface length. The shape of the domain means that hotspots get ‘trapped’ in convex regions.
